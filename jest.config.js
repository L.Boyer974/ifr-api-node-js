export default {
    "moduleFileExtensions": ["js", "json", "jsx", "ts", "tsx", "node"],
    "transform": {
        "^.+\\.jsx?$": "babel-jest",
        "^.+\\.tsx?$": "ts-jest"
    },
    "testRegex": "(/__tests__/.*|(\\.|/)(test|spec))\\.(jsx?|tsx?)$",
    "globals": {
        "ts-jest": {
            "tsconfig": "tsconfig.json"
        }
    }
}