import Joi from 'joi';

export const taskValidation = Joi.object({
    name: Joi.string().required(),
    description: Joi.string().required(),
    date: Joi.date(),
    completed: Joi.boolean()
});