import Joi from 'joi';

export const userValidation = Joi.object({
    name: Joi.string().required(),
    email: Joi.string().required(),
    password: Joi.string().required(),
    date: Joi.date(),
    state: Joi.boolean()
});