import express from 'express'
import { authMiddleware } from './middleware/auth.js';
import { getTask, getTasks, insertTask, removeTask, updateTask } from './controllers/TaskController.js'
import { getUser, getUsers, insertUser, removeUser, updateUser } from './controllers/UserController.js'
import { login } from './controllers/AuthController.js'
import { uploadFile } from './controllers/UploadController.js'

const router = express.Router();

router.get('/', (req, res) => {
    res.send({
        'message': 'Hello'
    })
})

router.get('/api/tasks', authMiddleware, getTasks)
router.post('/api/tasks', authMiddleware, insertTask)
router.put('/api/tasks/:id', authMiddleware, updateTask)
router.delete('/api/tasks/:id', authMiddleware, removeTask)
router.get('/api/tasks/:id', authMiddleware, getTask)


router.get('/api/users', getUsers)
router.post('/api/users', authMiddleware, insertUser)

/**
 * @api {get} /api/users/:id Récupérer les informations d'un utilisateur
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} id Identifiant unique de l'utilisateur
 *
 * @apiSuccess {String} firstname Prénom de l'utilisateur
 * @apiSuccess {String} lastname Nom de famille de l'utilisateur
 * @apiSuccess {String} email Adresse email de l'utilisateur
 */
router.put('/api/users/:id', authMiddleware, updateUser)
router.delete('/api/users/:id', authMiddleware, removeUser)
router.get('/api/users/:id', getUser)

router.post('/api/login', login)

router.post('/api/upload', uploadFile)

export default router