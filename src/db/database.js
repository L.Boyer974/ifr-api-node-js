import mongoose from 'mongoose';

export default class Database {
    constructor() {
        this.connect();
    }

    async connect() {
        try {
            await mongoose.connect('mongodb://localhost:27017/todolist');
        } catch (err) {
            console.error('Failed to connect to database:', err);
        }
    }
}
