import multer from "multer"
import path from "path"

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'tmp/uploads/')
    },
    filename: (req, file, cb) => {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        const extension = path.extname(file.originalname)
        const filename = `${file.fieldname}-${uniqueSuffix}${extension}`
        cb(null, filename)
    }
})

const fileFilter = (req, file, cb) => {
    if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/png') {
        cb(null, true)
    } else {
        cb(new Error('Seulement des images en format .jpeg ou png'), false)
    }
}

const upload = multer({ storage: storage, fileFilter: fileFilter })

export function uploadFile(req, res, next) {
    upload.single('image')(req, res, (err) => {
        if (err) {
            return next(err)
        }
    })

    // const fileUrl = `${req.protocol}://${req.hostname}:${process.env.PORT}/uploads/${req.file.filename}`

    res.json({
        message: "Image upload réussite",
        // url: fileUrl
    })
}
