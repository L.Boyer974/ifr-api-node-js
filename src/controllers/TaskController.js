import Task from '../models/Task.js'
import { taskValidation } from '../validators/task.js'

export async function getTasks(req, res) {
    try {
        const taskModel = new Task()
        const tasks = await taskModel.getAll({ assignedTo: req.user.id })
        res.status(200).json(tasks)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

export async function getTask(req, res) {
    try {
        const taskModel = new Task()
        const task = await taskModel.getById({ _id: req.params.id, assignedTo: req.user.id })
        if (!task) {
            res.status(404).json({ message: `La tâche avec ID ${req.params.id} n'existe pas` })
        }
        res.status(200).json(task)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

export async function insertTask(req, res) {
    try {
        const { error } = taskValidation.validate(req.body)
        if (error) {
            return res.status(400).send(error.details[0].message);
        }
        const taskModel = new Task()
        req.body['assignedTo'] = req.user.id
        const newTask = await taskModel.insert(req.body)
        res.status(201).json(newTask)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

export async function updateTask(req, res) {
    try {
        const taskModel = new Task()
        const updateTask = await taskModel.update({ _id: req.params.id }, req.body)
        if (!updateTask) {
            res.status(404).json({ message: `La tâche avec ID ${req.params.id} n'existe pas` })
        }
        res.status(204).json(updateTask)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

export async function removeTask(req, res) {
    try {
        const taskModel = new Task()
        const deleteTask = await taskModel.remove({ _id: req.params.id })
        if (!deleteTask) {
            res.status(404).json({ message: `La tâche avec ID ${req.params.id} n'existe pas` })
        }
        res.status(204).json(deleteTask)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}