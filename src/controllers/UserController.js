import User from '../models/User.js'
import { userValidation } from '../validators/user.js'
import argon2 from 'argon2'

export async function getUsers(req, res) {
    try {
        const userModel = new User()
        const users = await userModel.getAll()
        res.status(200).json(users)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

export async function getUser(req, res) {
    try {
        const userModel = new User()
        const user = await userModel.getById({ _id: req.params.id })
        if (!user) {
            res.status(404).json({ message: `L'utilisateur avec ID ${req.params.id} n'existe pas` })
        }
        res.status(200).json(user)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

export async function insertUser(req, res) {
    try {
        const { error } = userValidation.validate(req.body)
        if (error) {
            return res.status(400).send(error.details[0].message);
        }
        req.body['password'] = await argon2.hash(req.body['password'])
        const userModel = new User()
        const newUser = await userModel.insert(req.body)
        res.status(201).json(newUser)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

export async function updateUser(req, res) {
    try {
        const userModel = new User()
        if (req.body['password']) {
            req.body['password'] = await argon2.hash(req.body['password'])
        }
        const updateUser = await userModel.update({ _id: req.params.id }, req.body)
        if (!updateUser) {
            res.status(404).json({ message: `L'utilisateur avec ID ${req.params.id} n'existe pas` })
        }
        res.status(204).json(updateUser)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

export async function removeUser(req, res) {
    try {
        const userModel = new User()
        const deleteUser = await userModel.remove({ _id: req.params.id })
        if (!deleteUser) {
            res.status(404).json({ message: `La tâche avec ID ${req.params.id} n'existe pas` })
        }
        res.status(204).json(deleteUser)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}