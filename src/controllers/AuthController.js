import User from '../models/User.js'
import argon2 from 'argon2'
import { generateToken } from '../middleware/token.js'


export async function login(req, res) {
    try {
        const userModel = new User()
        const user = await userModel.getByEmail(req.body)
        if (!user) {
            res.status(404).json({ message: `L'utilisateur avec l'adresse ${req.body['email']} n'existe pas` })
        }
        const verifyPasswordHash = argon2.verify(user.password, req.body['password'])
        if (!verifyPasswordHash) {
            res.status(401).json({ message: `L'adresse email ou le mot de passe est incorrect` })
        }
        res.status(200).json({ token: generateToken(user._id) })
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}
