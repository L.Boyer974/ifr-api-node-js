import jwt from 'jsonwebtoken'

export function generateToken(userId) {
    const secretKey = process.env.JWT_SECRET
    const token = jwt.sign({ id: userId }, secretKey, { expiresIn: '1h' })
    return token
}