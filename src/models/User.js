import Crud from './Crud.js'

export default class User extends Crud {
    constructor() {
        super('user', {
            name: String,
            email: String,
            password: String,
            date: { type: Date, default: Date.now },
        })
    }

    async getAll() {
        const result = await this.model.find().select('name email').exec()
        return result
    }

    async getById(id) {
        const result = await this.model.findById(id).select('-password').exec()
        return result
    }


    async getByEmail(data) {
        const result = await this.model.findOne({ email: data['email'] }).exec()
        return result
    }
}