import Crud from './Crud.js'
import mongoose from 'mongoose'

export default class Task extends Crud {
    constructor() {
        super('task', {
            name: String,
            description: String,
            date: { type: Date, default: Date.now },
            completed: Boolean,
            assignedTo: mongoose.Schema.Types.ObjectId
        })
    }

    async getAll(userId) {
        const result = await this.model.find(userId).select('-assignedTo').exec()
        return result
    }

    async getById(id) {
        const result = await this.model.find(id).select('-assignedTo').exec()
        return result
    }
}