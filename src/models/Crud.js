import mongoose from 'mongoose';
import Database from '../db/database.js';

export default class Crud extends Database {
    constructor(modelIndex, schema) {
        super()
        const { Schema } = mongoose

        if (mongoose.models[modelIndex]) {
            delete mongoose.models[modelIndex]
        }

        const schemaModel = new Schema(schema)

        this.model = mongoose.model(modelIndex, schemaModel)
    }

    async getById(id) {
        const result = await this.model.findById(id).exec()
        return result
    }

    async getAll() {
        const result = await this.model.find().exec()
        return result
    }

    async insert(data) {
        const query = new this.model(data)
        const result = await query.save()

        return result
    }

    async update(id, data) {
        const result = await this.model.findByIdAndUpdate(id, data, { new: true })
        return result
    }

    async remove(id) {
        const result = await this.model.findByIdAndRemove(id).exec()

        return result
    }
}