import { buildSchema } from "graphql";

export const schema = buildSchema(`
    type Task {
        _id: ID!
        name: String!
        description: String!
        completed: Boolean!
        assignedTo: String!
    }

    type User {
        _id: ID!
        name: String!,
        email: String!
    }


    type Query {
        tasks:[Task!],
        users:[User!]
    }

    schema {
        query: Query
    }
`)