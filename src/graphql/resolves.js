import Task from '../models/Task.js'
import User from '../models/User.js'
import { taskValidation } from '../validators/task.js'

export const resolver = {
    tasks: async () => {
        try {
            const taskModel = new Task()
            const tasks = await taskModel.getAll()
            return tasks
        } catch (error) {
            console.error(error)
        }
    },
    // createNewTask: async args => {
    //     try {
    //         const { error } = taskValidation.validate(args)
    //         if (error) {
    //             return { error: error.details[0].message }
    //         }
    //         const taskModel = new Task()
    //         const newTask = await taskModel.insert(req.body)
    //     } catch (error) {
    //         console.error(error)
    //     }
    // },
    users: async () => {
        try {
            const userModel = new User()
            const users = await userModel.getAll()
            return users
        } catch (error) {
            console.error(error)
        }
    }
}