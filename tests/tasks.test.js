import { insertTask } from './controllers/TaskController.js'

describe('Task Controller', () => {
    describe('Create Task', () => {
        it('Should create a task successfully', async () => {
            const newTask = {
                title: 'Task 1 Updated',
                description: 'Description 1 Updated',
                completed: true
            }
            const req = {
                body: newTask
            }
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn()
            }
            await insertTask(req, res)
            expect(res.status).toHaveBeenCalledWith(204)
            expect(res.json).toHaveBeenCalledWith(newTask)
        })
    })
})