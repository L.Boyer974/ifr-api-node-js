import express from 'express'
import bodyParser from 'body-parser'
import apicache from 'apicache'
import routes from './src/routes.js'
import dotenv from 'dotenv'
import { graphqlHTTP } from "express-graphql"
import { schema } from './src/graphql/schema.js'
import { resolver } from './src/graphql/resolves.js'

dotenv.config()

const app = express()
const port = 3000

// Configure cache
// let cache = apicache.middleware
// app.use(cache('5 minutes'))

// Configure les params
app.use(bodyParser.json())
app.use(express.urlencoded({ extended: true }))

// Configuration apidoc
// app.use('/apidoc', express.static('apidoc', { headers: { 'Content-Type': 'text/plain' } }));
// app.disable('x-powered-by');

// Configuration header
app.use((req, res, next) => {
    res.type('application/json')
    next()
});

// Configure route
app.use(routes)

// PARTIR GRAPHQL
app.use(
    "/graphql",
    graphqlHTTP({
        schema: schema,
        rootValue: resolver,
        graphiql: true,
    })
)

// Configure server
app.listen(port, () => {
    console.log(`listen to ${port}`)
})