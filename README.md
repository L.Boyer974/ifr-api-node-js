
# Exemple de pour pour une api de liste de tâche + user
Cette api permet de récupérer une liste de tâche et ces listes d'utilisateurs. Il y a un système de middleware si vous souhaitez vous inspirez en terme de code.

## End point
### Tâches
|  HTTP| ROUTE | DESCRIPTION |
|--|--|--|
| GET | /api/tasks | Permet de récupères ensemble des tâches |
| GET | /api/tasks/:id | Permet de une tâche par rapport à son id |
| POST | /api/tasks | Permet de créer une nouvelle tâche |
| PUT | /api/tasks/:id | Permet de mettre à jour une tâche |
| DELETE | /api/tasks/:id | Permet de supprimer une tâche |

Structure de la données

    _id: ObjectID,
    name:  String,
    description:  String
    date:  Date
    completed:  Boolean
    assignedTo: ObjectID

### Utilisateurs

|  HTTP| ROUTE | DESCRIPTION |
|--|--|--|
| GET | /api/users | Permet de récupères ensemble des utilisateurs |
| GET | /api/users/:id | Permet de un utilisateur à son id |
| POST | /api/users | Permet de créer un nouveau utilisateur |
| PUT | /api/users/:id | Permet de mettre à jour un utilisateur |
| DELETE | /api/users/:id | Permet de supprimer un utilisateur |

Structure de la données

    _id: ObjectID,
    name:  String,
    email:  String
    password:  String
    date:  Date

### Auth

|  HTTP| ROUTE | DESCRIPTION |
|--|--|--|
| POST | /api/login | Permet de se connecter, renvoie un token |

### Upload

|  HTTP| ROUTE | DESCRIPTION |
|--|--|--|
| POST | /api/upload | Permet d'upload une image, en multipart/form |

### GraphQL (en cours de devéloppement)

L'api propose également la possibilité d'utiliser du GraphQL, pour pouvoir utiliser du GraphQL rendez-vous sur 

    /graphql

## Test (en cours de développement)

Vous pouvez lancer des tests unitaires utilisant la commande

    npm run test

## Installation

Pour installer le projet il suffit de faire un clone du projet et de lancer :

    npm install

Il est nécessaire d'avoir MongoDB installer sur sa machine

## Lancer le projet

Pour lancer le projet, il suffit de faire :

    npm run start

